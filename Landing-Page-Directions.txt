********************************************************************************
Theme Name:         Signature Healthcare
Theme URI:          http://responsivestyletil.es/signaturehc/
Description:        The is a landing page for Signature Healthcare
Version:            1.0.0
Author:             Clifton Canady
E-mail: 			hello@ctwebdesignshop.com
Twitter: 			@cliftoncanady
Facebook: 			http://facebook.com/CTWebDev
Author URI:         http://cliftoncanady.co/
Business URI:       http://ctwebdesignshop.com/
*********************************************************************************

--------------------------
- Landing Page Directions
--------------------------

************************************
** Technology 
************************************
	- Zurb Foundation 5
	- OWL Carousel
	- Font Awesome 4.1.0
	- Chris Coyier's Grunt Boilerplate
	- GruntJS
	- human.txt
	- robots.txt


************************************
** Revisions 
************************************
HTML files
----------
lp1.html, lp2.html,lp3.html, lp5.html, lp5.html
	- These are all revision of the index.html

************************************
** Files/Folders that need to upload
************************************
HTML
----------
index.html -- This is the html file with all the content.

CSS
----------
css folder --  all css files in the folder  

JS
----------
js folder -- all css files in the folder  

bower_components
----------
bower_components folder -- all css files/folder in the folder  

Images
----------
Every image in the folder need to be uploaded

- You might have to change the links of the images and background images used to suit your domain
	- Background of top hero section css image
	-------------------------------------------
	.jumbotron {
--->> background: url(/images/sch_bg.jpg) no-repeat center center fixed;  <<-----
	  -webkit-background-size: cover;
	  -moz-background-size: cover;
	  -o-background-size: cover;
	  background-size: cover;
	  padding: 2em 0;
	}

	- Section Header background line css image
	-------------------------------------------
	h1.header {
	  margin: 0;
	  text-transform: uppercase;
	  text-align: center;
--->> background: url(/images/h1_line.png) repeat-x right center; <<---
	  font-size: 32px;
	  line-height: 66px;
	  color: #3e3e3e;
	  font-weight: 600;
	}

