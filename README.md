CTWD Fiverr Landing Pages
==========================

This is a repo for my CTWD Fiverr Landing Pages that I creating with [Zurb Foundation 5](http://foundation.zurb.com/ "Zurb Foundation 5"), plus a bit more.

## Technology
* [Zurb Foundation 5](http://foundation.zurb.com/ "Zurb Foundation 5")
* [OWL Carousel](http://owlgraphic.com/owlcarousel/)
* [Font Awesome 4.1.0](http://fortawesome.github.io/Font-Awesome/)
* [Chris Coyier's Grunt Boilerplate](https://github.com/chriscoyier/My-Grunt-Boilerplate/)
* [GruntJS](http://gruntjs.com/)
* [human.txt](http://humanstxt.org/Standard.html)
* [robots.txt](http://robotstxt.org/Standard.html)
* [Google Analytics](https://www.google.com/analytics)

## Contact
#### Developer/Projects spokesman
* Homepage: [ctwebdesignshop.com](http://ctwebdesignshop.com/)
* e-mail: hello@ctwebdesignshop.com
* Twitter: [@cliftoncanady](https://twitter.com/twitterhandle "@cliftoncanady on twitter")
* Facebook: [http://facebook.com/CTWebDev](http://facebook.com/CTWebDev "CTWebDev on facebook")