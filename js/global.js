$("#owl-demo").owlCarousel({

  autoPlay: 3000,

  items : 2,
  lazyLoad : true,

  itemsDesktop : [1199,3],
  itemsDesktopSmall : [979,3]

   	  // navigation : true, // Show next and prev buttons
      // slideSpeed : 300,
      // paginationSpeed : 400,
      // singleItem:true
 
      // // "singleItem:true" is a shortcut for:
      // // items : 1, 
      // // itemsDesktop : false,
      // // itemsDesktopSmall : false,
      // // itemsTablet: false,
      // // itemsMobile : false

});



$(document).foundation();

var doc = document.documentElement;
doc.setAttribute('data-useragent', navigator.userAgent);

$(document).on('close.fndtn.alert-box', function(event) {
  console.info('An alert box has been closed!');
});


$(function(){
  $('#topnav ul li a').on('click', function(e){
    e.preventDefault();
    var scrolldiv = $(this).attr('href');
    
    $(scrolldiv).animatescroll({padding:50});
  });
});


  $(document).ready(function() {
  
    $("#countdown").countdown({
      date: "25 december 2014 12:00:00", // add the countdown's end date (i.e. 3 november 2012 12:00:00)
      format: "on" // on (03:07:52) | off (3:7:52) - two_digits set to ON maintains layout consistency
    },
    
    function() { 
      
      // the code here will run when the countdown ends
      alert("done!") 

    });
  });
